const fastify = require('fastify')({ logger: true });
const prisma = require('@prisma/client');
const axios = require('axios');

fastify.post('/register', async (request, reply) => {
    try {
        const { username, password, bankid, accountname, accountnumber, phonenumber } = request.body;

        // ส่งข้อมูลไปยัง API ของ Agent
        const agentResponse = await axios.post('https://ggapi-uat.5k2an3or4q209.xyz/ggapi/register', {
            username,
            password,
            bankid,
            accountname,
            accountnumber,
            phonenumber
        });

        // บันทึกข้อมูลลงในฐานข้อมูล
        const user = await prisma.user.create({
            data: {
                username,
                password,
                bankid,
                accountname,
                accountnumber,
                phonenumber
            }
        });

        reply.send({ user, agentResponse });
    } catch (error) {
        reply.status(500).send({ error: 'Internal Server Error' });
    }
});

const start = async () => {
    try {
        await fastify.listen(3000);
        fastify.log.info(`Server is listening on ${fastify.server.address().port}`);
    } catch (err) {
        fastify.log.error(err);
        process.exit(1);
    }
};

start();
