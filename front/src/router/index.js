// router/index.js
import { createRouter, createWebHistory } from 'vue-router';
import MainLayout from '@/components/MainLayout.vue';

import HomePage from '../views/HomePage.vue'; // Import your default page component
import LoginForm from '@/components/LoginForm.vue';
import RegisterForm from '../components/RegisterForm.vue';
import DepositForm from '../components/DepositForm.vue';
import WithdrawForm from '../components/WithdrawForm.vue';


const routes = [
  {
    path: '/',
    component: MainLayout,
    children: [
      {
        path: '/',
        name: 'Home',
        component: HomePage // Use your default page component here
      },
      {
        path: '/login',
        name: 'Login',
        component: LoginForm // Use your default page component here
      },
      {
        path: '/register',
        name: 'Register',
        component: RegisterForm
      },
      {
        path: '/deposit',
        name: 'Deposit',
        component: DepositForm
      },
      {
        path: '/withdrawal',
        name: 'Withdrawal',
        component: WithdrawForm
      },
      // Define other routes here...
    ]
  },
  
];

const router = createRouter({
  history: createWebHistory(),
  routes
});

export default router;
