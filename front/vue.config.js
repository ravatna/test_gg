const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,

  pluginOptions: {
    vuetify: {
			// https://github.com/vuetifyjs/vuetify-loader/tree/next/packages/vuetify-loader
		}
  },

  devServer: {
    proxy: {
      '/ggapi': {
        target: 'https://ggapi-uat.5k2an3or4q209.xyz',
        changeOrigin: true,
        pathRewrite: { '^/ggapi': '' },
      },
    },
  },
});
